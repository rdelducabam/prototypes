import boto3
import logging
import magic
import PIL

from io import BytesIO
from os import path, getenv
from PIL import Image

supportedFormats = [ 'JPEG', 'GIF', 'BMP', 'EPS', 'JPEG 2000', 'PCX', 'PNG', 'TIFF' ]

#### Defaults
thumbWidth = 300
thumbHeight = 300
thumbPrefix = 't_'
maxWidth = 1200
maxHeight = 1200


s3 = boto3.resource('s3')

logger = logging.getLogger('UploadPreprocessor')
if getenv('DEBUG'):
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.ERROR)


def lambda_handler(event, context):

    override_defaults()

    logger.info("Processing key %s" % event['UploadedKey'])

    key = s3.Object( bucket_name=getenv('SOURCE_BUCKET'), key=event['UploadedKey'] )
    imageSource = key.get()['Body'].read()
    imageStream = Image.open(BytesIO(imageSource))
    format = imageStream.format

    if format in supportedFormats:
        write_thumbnail(event['UploadedKey'], event['TargetKey'] )
        write_scaled(event['UploadedKey'], event['TargetKey'] )
    else:
        logger.info("%s not an image. Passing through" % event['UploadedKey'])

        origKey = {
            'Bucket': getenv('SOURCE_BUCKET'),
            'Key': event['UploadedKey']
         }

        tBucket = s3.Bucket(getenv('TARGET_BUCKET'))
        tBucket.copy(origKey, event['TargetKey'])

        s3.Object(getenv('SOURCE_BUCKET'),event['UploadedKey']).delete()
        logger.info("Moved key %s/%s --> %s/%s",
            getenv('SOURCE_BUCKET'),
            event['UploadedKey'],
            getenv('TARGET_BUCKET'),
            event['TargetKey'],
        )

def write_thumbnail(sourceKey, targetKey):
    # DEMO
    key = s3.Object( bucket_name=getenv('SOURCE_BUCKET'), key=sourceKey )
    imageSource = key.get()['Body'].read()
    imageStream = Image.open(BytesIO(imageSource))
    format = imageStream.format
    imageStream = imageStream.resize((thumbWidth,thumbHeight), PIL.Image.ANTIALIAS)

    outStream = BytesIO()
    imageStream.save(outStream,format)
    outStream.seek(0)

    ### TODO: Attach the prefix to the final path componetn ONLY
    ### This DEMO code assumes the image is in the root of the bucket
    thumb = s3.Object( bucket_name=getenv('TARGET_BUCKET'), key=thumbPrefix + targetKey )
    thumb.put(Body=outStream)
    logger.info("Wrote thumbnail to %s/%s", getenv('TARGET_BUCKET'), thumbPrefix + targetKey)
    return()

def write_scaled(sourceKey, targetKey):
    key = s3.Object( bucket_name=getenv('SOURCE_BUCKET'), key=sourceKey )
    imageSource = key.get()['Body'].read()
    imageStream = Image.open(BytesIO(imageSource))
    format = imageStream.format
    width, height = imageStream.size

    if width > maxWidth or height > maxHeight:
        ratio = min( maxWidth/width, maxHeight/height )
        imageStream = imageStream.resize(( int(width * ratio),int(height * ratio)), PIL.Image.ANTIALIAS)
        logger.info("Resized image %s" % sourceKey)
    outStream = BytesIO()
    imageStream.save(outStream,format)
    outStream.seek(0)

    processed = s3.Object( bucket_name=getenv('TARGET_BUCKET'), key=targetKey )
    processed.put(Body=outStream)
    logger.info("moveh image to %s/%s", getenv('TARGET_BUCKET'), targetKey)
    s3.Object(getenv('SOURCE_BUCKET'),sourceKey).delete()
    return()



def override_defaults():
    if getenv('THUMB_WIDTH'):
        thumbWidth = int( getenv('THUMB_WIDTH'))

    if getenv('THUMB_HEIGHT'):
        thumbHeight = int( getenv('THUMB_HEIGHT'))

    if getenv('MAX_WIDTH'):
        maxWidth = int( getenv('MAX_WIDTH'))

    if getenv('MAX_HEIGHT'):
        maxHeight = int( getenv('MAX_HEIGHT'))
